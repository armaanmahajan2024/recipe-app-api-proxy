# Recipe app API proxy

NGINX proxy app for our recipe app API

## Usage 

## Environment variables

* `LISTEN_PORT` - Port to listen on default : 8080

* `APP_HOST` - Hostname of the app to forward requests : app
* `APP_PORT` - port of the app to forward requets : 9000
